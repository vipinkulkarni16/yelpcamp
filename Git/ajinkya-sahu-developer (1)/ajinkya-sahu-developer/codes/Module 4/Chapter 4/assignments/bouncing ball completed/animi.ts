var canvas: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context: CanvasRenderingContext2D = canvas.getContext("2d");
var c:Geometry.Circle;
var p1= new Geometry.Point(500,50);
var b:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
function bounce(){
    var b1=parseFloat(b.value);
    if((b1>=0) && (b1<=0.98) && (isNaN(b1)!=true)){
        c=new Geometry.Circle(p1,25,"red",b1,context);
        c.draw();
        animate();
    }
    else{
        alert("Entered number is not between the range 0 to 0.98 or it is not a number")
    }
}
function animate(){
    context.clearRect(0,0,canvas.width,canvas.height);
    c.draw();
    c.update();
    window.requestAnimationFrame(animate);
}