var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
canvas.addEventListener("click", circle);
function circle(e) {
    var p = new Geometry.point(e.clientX, e.clientY);
    var cir = new Geometry.circle(context, p, 30, "red");
    cir.draw();
}
//# sourceMappingURL=app.js.map