class point
    {
        public x:number;
        public y:number;
        
        constructor(x:number,y:number)
        {
            this.x = x;
            this.y = y;
        }
    }

class line { //line class

        public pt1x:number;
        public pt1y:number;
        public pt2x:number;
        public pt2y:number;
        public context:CanvasRenderingContext2D;
        private ang:number;
        public color:string;

        constructor(context:CanvasRenderingContext2D,pt1x:number,pt1y:number,pt2x:number,pt2y:number,color:string){

            this.pt1x = pt1x;
            this.pt2x = pt2x;
            this.pt1y = pt1y;
            this.pt2y = pt2y;
            this.context = context;
            this.color=color;


        }

        draw(){ //drawing line with function

        this.context.beginPath();
        this.context.moveTo(this.pt1x, this.pt1y);
        this.context.lineTo(this.pt2x, this.pt2y);
        this.context.lineWidth = 4;
        this.context.strokeStyle = this.color;
        this.context.stroke();

        }
    }
    
class circle { //declaring a class circle

        public pt1:point;
        public context:CanvasRenderingContext2D;
        public r:number;
        public color: string;


        constructor(context:CanvasRenderingContext2D,pt1:point,r:number,color:string){

            this.pt1 = pt1;
            this.context = context;
            this.r=r;
            this.color=color;
            
        }
        // drawing circle
        draw(){

            this.context.beginPath();
            this.context.arc(this.pt1.x, this.pt1.y, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 2;
            this.context.fillStyle = this.color;
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
        
        }
        isinside(pt:point): boolean{
            let rad =Math.sqrt(Math.pow(pt.x-this.pt1.x,2)+Math.pow(pt.y-this.pt1.y,2));
            if(rad<this.r){
                return (true);
            }
            else{
                return(false);
            }
        }
        set cenpoint(pt:point){
            this.pt1=pt;
        }
    }

class polygon 
    {
        public pt2:point;
        public context:CanvasRenderingContext2D;
        public r:number;
        public color: string;
        constructor(context:CanvasRenderingContext2D,pt2:point,sides:number,color:string){
            this.pt2 = pt2;
            this.context = context;
            this.r=sides;
            this.color=color;
        }
        // drawing circle
        draw(){
            this.context.beginPath();
            this.context.moveTo( this.pt2.x+50 ,this.pt2.y);
            console.log("wrk 1");
            for (var i=1; i<=this.r;i++)
            {
                var x1 = this.pt2.x +50 * Math.cos((360/this.r) * i *Math.PI/180 );  
                var y1 = this.pt2.y +50 * Math.sin(360/this.r * Math.PI/180 * i);                  
                this.context.lineTo(x1,y1);
                console.log(x1,y1);
            }
            console.log("wrk 2");
            //this.context.closePath();
            this.context.lineWidth = 2;
            this.context.fillStyle = this.color;
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
            console.log("wrk 3");
        
        }
        isinside(pt:point): boolean{
            let rad =Math.sqrt(Math.pow(pt.x-this.pt2.x,2)+Math.pow(pt.y-this.pt2.y,2));
            if(rad<50){
                return (true);
            }
            else{
                return(false);
            }
        }
        set startpoint(pt:point){
            this.pt2=pt;
        }
    }
