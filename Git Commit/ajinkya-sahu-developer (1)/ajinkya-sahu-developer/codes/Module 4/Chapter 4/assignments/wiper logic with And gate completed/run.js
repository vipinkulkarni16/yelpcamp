var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var p1 = new Geometry.point(300, 300);
var p2 = new Geometry.point(320, 300);
context.translate(0, 600);
context.scale(1, -1);
var c1 = new Geometry.circle(context, p2, p1, 30, "white");
var c2 = new Geometry.circle(context, p1, p2, 10, "yellow");
c1.draw();
//tsc -p tsconfig.jsonc2.draw();
var s3 = new Geometry.point(400, 260);
var rect = new Geometry.rectangle(context, s3, 10, 80, "red");
rect.draw();
var l1 = new Geometry.point(405, 280);
var l2 = new Geometry.point(450, 280);
var l3 = new Geometry.point(405, 320);
var l4 = new Geometry.point(450, 320);
var line1 = new Geometry.line(context, l1, l2, "black");
var line2 = new Geometry.line(context, l3, l4, "black");
line1.draw();
line2.draw();
var s1 = 0;
var s2 = 0;
var result = false;
var p3 = new Geometry.point(50, 250);
var p4 = new Geometry.point(50, 350);
var c3 = new Geometry.circle(context, p3, p4, 20, "grey");
var c4 = new Geometry.circle(context, p4, p3, 20, "grey");
c3.draw();
c4.draw();
var p11 = new Geometry.point(70, 250);
var p12 = new Geometry.point(125, 250);
var p13 = new Geometry.point(125, 285);
var p14 = new Geometry.point(200, 285);
var line11 = new Geometry.line(context, p11, p12, "black");
var line12 = new Geometry.line(context, p12, p13, "black");
var line13 = new Geometry.line(context, p13, p14, "black");
line11.draw();
line12.draw();
line13.draw();
var p21 = new Geometry.point(70, 350);
var p22 = new Geometry.point(125, 350);
var p23 = new Geometry.point(125, 315);
var p24 = new Geometry.point(200, 315);
var line21 = new Geometry.line(context, p21, p22, "black");
var line22 = new Geometry.line(context, p22, p23, "black");
var line23 = new Geometry.line(context, p23, p24, "black");
line21.draw();
line22.draw();
line23.draw();
var p31 = new Geometry.point(200, 275);
var p32 = new Geometry.point(200, 325);
var p33 = new Geometry.point(220, 275);
var p34 = new Geometry.point(220, 325);
var line31 = new Geometry.line(context, p31, p32, "black");
var line32 = new Geometry.line(context, p31, p33, "black");
var line33 = new Geometry.line(context, p32, p34, "black");
line31.draw();
line32.draw();
line33.draw();
var semi = new Geometry.arc(context, 220, 300, 25);
semi.draw();
var p41 = new Geometry.point(245, 300);
var p42 = new Geometry.point(290, 300);
var line41 = new Geometry.line(context, p41, p42, "black");
line41.draw();
function switch1() {
    if (s1 == 0) {
        s1 = 1;
    }
    else {
        s1 = 0;
    }
    andgate();
}
function switch2() {
    if (s2 == 0) {
        s2 = 1;
    }
    else {
        s2 = 0;
    }
    andgate();
}
function andgate() {
    if (s1 == 1 && s2 == 1) {
        result = true;
    }
    else {
        result = false;
    }
    if (s1 == 1) {
        c4.color = "green";
        line21.color = "green";
        line22.color = "green";
        line23.color = "green";
    }
    else if (s1 == 0) {
        c4.color = "grey";
        line21.color = "grey";
        line22.color = "grey";
        line23.color = "grey";
    }
    if (s2 == 1) {
        c3.color = "green";
        line11.color = "green";
        line12.color = "green";
        line13.color = "green";
    }
    else if (s2 == 0) {
        c3.color = "grey";
        line11.color = "grey";
        line12.color = "grey";
        line13.color = "grey";
    }
    animate();
}
function animate() {
    if (result) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        c1.draw();
        c1.color = "blue";
        c2.draw();
        c2.updateangle();
        line1.draw();
        line1.updateangle();
        line2.draw();
        line2.updateangle();
        rect.draw();
        c3.draw();
        c4.draw();
        line11.draw();
        line12.draw();
        line13.draw();
        line21.draw();
        line22.draw();
        line23.draw();
        line31.draw();
        line32.draw();
        line33.draw();
        semi.draw();
        line41.draw();
        window.requestAnimationFrame(animate);
    }
    else {
        context.clearRect(0, 0, canvas.width, canvas.height);
        c1.draw();
        c1.color = "white";
        line1.draw();
        line2.draw();
        rect.draw();
        c3.draw();
        c4.draw();
        line11.draw();
        line12.draw();
        line13.draw();
        line21.draw();
        line22.draw();
        line23.draw();
        line31.draw();
        line32.draw();
        line33.draw();
        semi.draw();
        line41.draw();
    }
}
canvas.addEventListener("click", mouse, true);
function mouse(e) {
    var rect = canvas.getBoundingClientRect();
    var x1 = e.clientX - rect.x;
    var y1 = 600 - (e.clientY - rect.y);
    var a = Math.pow(p3.x - x1, 2) + Math.pow(p3.y - y1, 2);
    a = Math.sqrt(a);
    console.log("mouse", x1, y1);
    if (a < 20) {
        switch2();
    }
    var b = Math.pow(p4.x - x1, 2) + Math.pow(p4.y - y1, 2);
    b = Math.sqrt(b);
    console.log("mouse2", x1, y1, b, a);
    if (b < 20) {
        switch1();
    }
}
//# sourceMappingURL=run.js.map