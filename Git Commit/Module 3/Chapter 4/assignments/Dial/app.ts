var canvas: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context: CanvasRenderingContext2D = canvas.getContext("2d");

context.translate(300,300);

var c1 = new Geometry.circle(context,0,0,150,"red");
c1.draw();


var l1 = new Geometry.line(context,0,0,100,0,"blue");
l1.draw();

write();

function move(){

    context.clearRect(-300,-300,canvas.width,canvas.height);

    var value:HTMLInputElement =<HTMLInputElement> document.getElementById("t1");
    var val = parseFloat(value.value);

    var x = 130 * Math.cos(-val * Math.PI / 180);
    var y = 130 * Math.sin(-val * Math.PI / 180);
    
    c1.draw();
    var l1 = new Geometry.line(context,0,0,x,y,"green");
    l1.draw();
    write();
    
    context.fillText("Value on range Slider",0,-250,500);
    context.fillText(value.value,100,-250,350);


}

function write(){

    context.fillText("0",180,0,100);
    context.fillText("90",0,-180,100);
    context.fillText("180",-180,0,100);
    context.fillText("270",0,180,100);
    context.fillText("315",180 * Math.cos(Math.PI/4),180 * Math.sin(Math.PI/4),100);
    context.fillText("225",180 * Math.cos(3*Math.PI/4),180 * Math.sin(3*Math.PI/4),100);
    context.fillText("135",180 * Math.cos(5*Math.PI/4),180 * Math.sin(5*Math.PI/4),100);
    context.fillText("45",180 * Math.cos(-Math.PI/4),180 * Math.sin(-Math.PI/4),100);
   
}