var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var p1 = new Geometry.point(200, 300);
var sinewave = new Geometry.sin(context, p1, "red");
sinewave.draw();
var cir = new Geometry.circle(context, p1, 10, "green");
function Anim() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    sinewave.draw();
    cir.draw();
    cir.update();
    window.requestAnimationFrame(Anim);
}
//# sourceMappingURL=app.js.map