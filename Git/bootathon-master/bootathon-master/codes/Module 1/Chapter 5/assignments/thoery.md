## Theory 
 
 * Procedure 
 
 1. Perform the partial pivoting of equation i.e. arranging the equation such that digonal element has maximum value. 

 2.   Rewrite  the equation 1<sup>st</sup> as   
 X<sub>1</sub>=f(x<sub>2</sub> , x<sub>3</sub> ,......x<sub>n</sub>)  
 Rewrite n<sup>th</sup>equation as   
 x<sub>n</sub>=f(x<sub>1</sub>,x<sub>2</sub>....x<sub>n-1</sub>)  
 These are known as characteristic equation  


 3.  Approximate initial solution as x<sub>1</sub>=0 , x<sub>2</sub>=0 , x<sub>n</sub>=0.(Values taken arbitarily)  
 4.  Put initial value in equation (i) to obtain x<sub>1</sub>. Now use the latest value in all next equation to get x<sub>2</sub> , x<sub>3</sub> etc . Now it is improved solution .  

 5.  Repeat step 4 with new values till desired accuracy is achieved i.e. difference between previous value of x and this value of x is minimum.  
   
 
 Example  
 
 
 x1 + 20x2 + 9x3 = ‐23  
2x1- 7x2 - 20x3 = ‐57  
20x1 + 2x2 + 6x3 = 28

 Partial Pivoting :   
 20x1 + 2x2 + 6x3 = 28...(1)  
x1 + 20x2 + 9x3 = ‐23...(2)   
2x1 - 7x2 - 20x3 = ‐57...(3)


Rearranging the equations :  
x1 = (28 ‐ 2x2 ‐ 6x3 )/20    
x2 = (‐23 - x1 - 9x3 )/20   
x3 = (‐57 - 2x1 + 7x2 )/(‐20)

Let initial approximation be x1 = 0, x2 = 0, x3 = 0, x4 = 0.    


|Iteration|X1|X2|X3|  
|----|---|---|---|---|---|
1|1.400000|-1.220000|3.417000|
2|0.496900|-2.712495|3.849063|
 3|0.516531|-2.907905|3.919420|
 4|0.514965|-2.939487|3.930317|
5|0.514854|-2.944385|3.932020|
6|0.514832|-2.945151|3.932286|

 

```
function gausseidel

         clc;

         clear;
    [a,c,x]=read_equation;

     [a,c]=pivote(a,c);

     x=iterate(a,c,x);

end

 

function [a,c,x]=read_equation

         n=input('\nEnter no of equation:');

         for i=1:1:n

         for j=1:1:n

        fprintf('\na[%d][%d]=',i,j);

        a(i,j)=input('');

   end

   fprintf('\nc[%d]=',i);

   c(i)=input('');

    x(i)=0;

  end

end

 

function [a,c]=pivote(a,c)

        n=length(c);

       for i=1:1:n-1

              for k=i+1:1:n

      if (abs(a(k,i))>abs(a(i,i)))

            for j=1:1:n

            temp=a(i,j);

            a(i,j)=a(k,j);

            a(k,j)=temp;

          end

         temp=c(i);

         c(i)=c(k);

         c(k)=temp;

         end

    end

 end

end

 

function x=iterate(a,c,x)

       n=length(c);

       acc=input('\nEnter accuracy to be achived:');

       err=1;

        k=1;

     while (abs(err)>acc)

        fprintf('\n%d ',k);

        for i=1:1:n

               temp=c(i);

             for j=1:1:n

                   if (i~=j)

           temp=temp-a(i,j)*x(j);

            end

    end

     err=x(i)-temp/a(i,i);

     x(i)=temp/a(i,i);

     fprintf('%f ',x(i));

      end

      k=k+1;

    end

end
```








 

 



 