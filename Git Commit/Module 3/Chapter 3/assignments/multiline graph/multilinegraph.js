var datapoints1 = [];
var datapoints2 = [];
for (var i = 0; i <= 10; i = +2) {
    datapoints1.push({
        x: i,
        y: Math.pow(i, 3),
    });
    datapoints2.push({
        x: i,
        y: Math.pow(i, 3),
    });
}
var data = [];
data.push({
    type: "line",
    xValueType: "Float",
    showInLegend: false,
    name: "f(x)",
    markerSize: 1,
    dataPoints: datapoints1
});
data.push({
    type: "line",
    xValueType: "Float",
    showInLegend: false,
    name: "f(x1)",
    markerSize: 1,
    dataPoints: datapoints2
});
graphline("l1", data, "x axis", "y-axis");
//# sourceMappingURL=multilinegraph.js.map