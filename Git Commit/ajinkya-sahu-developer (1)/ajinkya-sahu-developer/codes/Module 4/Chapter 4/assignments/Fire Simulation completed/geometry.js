var Geometry;
(function (Geometry) {
    class point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    Geometry.point = point;
    class circle {
        constructor(context, start_point, vx, vy, radius, color) {
            this.context = context;
            this.startpoint = start_point;
            this.vx = vx;
            this.vy = vy;
            this.color = color;
            this.alpha = 1;
            this.radius = radius;
        }
        draw() {
            console.log("1");
            this.context.save();
            this.context.beginPath();
            this.context.arc(this.startpoint.x, this.startpoint.y, this.radius, 0, Math.PI * 2);
            this.context.fillStyle = this.color;
            this.context.globalAlpha = this.alpha;
            this.context.fill();
            this.context.restore();
            this.update();
        }
        update() {
            this.startpoint.x = this.startpoint.x + this.vx;
            this.startpoint.y = this.startpoint.y + this.vy;
            this.alpha *= 0.98;
            console.log("2");
        }
        get Alpha() {
            return (this.alpha);
        }
    }
    Geometry.circle = circle;
    class smoke {
        constructor(canvas, context, pt, noofparticles, color) {
            this.container = [];
            this.canvas = canvas;
            this.context = context;
            this.pt = pt;
            this.num = noofparticles;
            this.vxrange = 3;
            this.vymin = 1;
            this.vymax = 15;
            this.radiusmin = 1;
            this.radiusmax = 15;
            this.color = color;
        }
        random(min, max) {
            return Math.round(Math.random() * (max - min) + min);
        }
        start() {
            this.context.save();
            this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.context.translate(0, canvas.height);
            this.context.scale(1, -1);
            for (let i = 1; i <= this.num; i++) {
                var vy = this.random(this.vymin, this.vymax);
                var vx = this.random(-this.vxrange, this.vxrange);
                var r = this.random(this.radiusmin, this.radiusmax);
                var cir = new circle(this.context, new point(this.pt.x, this.pt.y), vx, vy, r, this.color);
                this.container.push({ circle: cir });
                //console.log(this.container);
            }
            for (let j = 0; j < this.container.length; j++) {
                this.container[j].circle.draw();
            }
            this.context.restore();
            for (var i = 0; i < this.container.length; i++) {
                if (this.container[i].circle.Alpha < 0.1) {
                    this.container.splice(i, 1);
                    i--;
                }
            }
        }
    }
    Geometry.smoke = smoke;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map