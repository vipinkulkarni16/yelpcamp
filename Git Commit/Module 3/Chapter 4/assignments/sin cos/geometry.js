var Geometry;
(function (Geometry) {
    class line {
        constructor(context, pt1x, pt1y, pt2x, pt2y, color) {
            this.pt1x = pt1x;
            this.pt2x = pt2x;
            this.pt1y = pt1y;
            this.pt2y = pt2y;
            this.context = context;
            this.ang = -20;
            this.color = color;
            this.up = true;
            this.down = false;
        }
        draw() {
            this.context.beginPath();
            this.context.moveTo(this.pt1x, this.pt1y);
            this.context.lineTo(this.pt2x, this.pt2y);
            this.context.lineWidth = 2;
            this.context.strokeStyle = this.color;
            this.context.stroke();
        }
    }
    Geometry.line = line;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map