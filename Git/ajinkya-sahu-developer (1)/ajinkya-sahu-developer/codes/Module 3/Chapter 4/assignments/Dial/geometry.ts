namespace Geometry{

    export class line { //line class

        public pt1x:number;
        public pt1y:number;
        public pt2x:number;
        public pt2y:number;
        public context:CanvasRenderingContext2D;
        private ang:number;
        public color:string;

        constructor(context:CanvasRenderingContext2D,pt1x:number,pt1y:number,pt2x:number,pt2y:number,color:string){

            this.pt1x = pt1x;
            this.pt2x = pt2x;
            this.pt1y = pt1y;
            this.pt2y = pt2y;
            this.context = context;
            this.color=color;


        }

        draw(){ //drawing line with function

        this.context.beginPath();
        this.context.moveTo(this.pt1x, this.pt1y);
        this.context.lineTo(this.pt2x, this.pt2y);
        this.context.lineWidth = 4;
        this.context.strokeStyle = this.color;
        this.context.stroke();

        }
    }
    
    export class circle { //declaring a class circle

        private ang:number;
        public pt1:number;
        public pt2:number;
        public context:CanvasRenderingContext2D;
        public r:number;
        public color: string;


        constructor(context:CanvasRenderingContext2D,x:number,y:number,r:number,color:string){

            this.pt1 = x;
            this.pt2 = y;
            this.context = context;
            this.ang =0;
            this.r=r;
            this.color=color;
            
        }
        // drawing circle
        draw(){

            this.context.beginPath();
            this.context.arc(this.pt1, this.pt2, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 2;
            //this.context.fillStyle = this.color;
            //this.context.fill();
            this.context.strokeStyle = this.color;
            this.context.stroke();
        
        }

    }

}
