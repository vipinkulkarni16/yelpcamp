var canvas:HTMLCanvasElement =<HTMLCanvasElement> document.getElementById("mycanvas");
var context:CanvasRenderingContext2D =<CanvasRenderingContext2D> canvas.getContext("2d");

var p1 = new Geometry.point(300, 300);
var p2 = new Geometry.point(450, 300);
var l1 = new Geometry.line(context,p1,p2,"red");
l1.draw();
var run:boolean;


function animate() {
    if(run){
        context.clearRect(0, 0, canvas.width, canvas.height);
        l1.draw();
        console.log(p1,p2);
        window.requestAnimationFrame(animate);
    }
}

function stop_anim (){
    run = false;
    animate();
}

function start(){
    run = true;
    animate();
}