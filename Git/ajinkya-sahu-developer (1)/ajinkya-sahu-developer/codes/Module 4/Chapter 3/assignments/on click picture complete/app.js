var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var p1 = document.getElementById("pic1");
var p2 = document.getElementById("pic2");
var value;
canvas.addEventListener("click", circle);
function circle(e) {
    if (value) {
        context.drawImage(p1, e.clientX, e.clientY);
    }
    else if (value == false) {
        context.drawImage(p2, e.clientX, e.clientY);
    }
}
function pic1() {
    value = true;
}
function pic2() {
    value = false;
}
//# sourceMappingURL=app.js.map