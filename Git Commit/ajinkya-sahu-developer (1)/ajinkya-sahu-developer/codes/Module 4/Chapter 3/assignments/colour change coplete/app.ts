var canvas:HTMLCanvasElement =<HTMLCanvasElement> document.getElementById("mycanvas");
var context:CanvasRenderingContext2D =<CanvasRenderingContext2D> canvas.getContext("2d");

canvas.addEventListener("click",click);

var choice:string;

function circle() 
{
    choice="circle";
}

function triangle() 
{
    choice="triangle";
}

function square() 
{
    choice="square";
}

function pentagon()
{
    choice="pentagon";
}

function change_color(){

    choice = "change"
}

//defining container to store center point
var container:Geometry.circle[] = [];
var container1:Geometry.polygon[] = [];

//color container....
var colorcontainer = ["blue","green","brown","yellow","orange","pink","white","red","black"];


function click(e:MouseEvent)
{
    var p = new Geometry.point(e.clientX, e.clientY);
    console.log(e.clientX, e.clientY);

    if(choice=="circle")
    {
        var cir = new Geometry.circle(context,p, 30, "red");
        cir.draw();
        container.push(cir);
        console.log(container);

    }else if(choice=="triangle")
    {
        var tri = new Geometry.polygon(context,p.x,p.y,3,"green");
        tri.draw();
        container1.push(tri);
        console.log(container1);
    }else if(choice=="square")
    {
        var sq = new Geometry.polygon(context,p.x,p.y,4,"blue");
        sq.draw();
        container1.push(sq);
        console.log(container1);
    }else if(choice=="pentagon")
    {
        var pent = new Geometry.polygon(context,p.x,p.y,5,"yellow");
        pent.draw();
        container1.push(pent);
        console.log(container1);
    }else if(choice=="change")
    {
        for(var i=0;i<container.length;i++)
        {
            if(container[i].isinside(p))
            {
                container[i].color = colorcontainer[random(0,9)];  
            }
        }
        for(var i=0;i<container1.length;i++){
            if(container1[i].isinside(p))
            {
                container1[i].color = colorcontainer[random(0,9)];  
            }
        }
        drawagain();
    }
    
}

function drawagain(){
    context.clearRect(0,0,canvas.width,canvas.height)
    for(var i=0;i<container.length;i++){
        container[i].draw();
    }
    for(var i=0;i<container1.length;i++){
        container1[i].draw();
    }
}

function random(min:number,max:number)
{
    return Math.round(Math.random() * (max-min) + min);
}