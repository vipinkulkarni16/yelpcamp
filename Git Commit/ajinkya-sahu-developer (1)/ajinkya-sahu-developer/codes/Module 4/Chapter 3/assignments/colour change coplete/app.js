var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
canvas.addEventListener("click", click);
var choice;
function circle() {
    choice = "circle";
}
function triangle() {
    choice = "triangle";
}
function square() {
    choice = "square";
}
function pentagon() {
    choice = "pentagon";
}
function hexagon() {
    choice = "hexagon";
}
function octagon() {
    choice = "octagon";
}
function change_color() {
    choice = "change";
}
var container = [];
var container1 = [];
var colorcontainer = ["blue", "green", "brown", "yellow", "orange", "pink", "white"];
function click(e) {
    var p = new Geometry.point(e.clientX, e.clientY);
    console.log(e.clientX, e.clientY);
    if (choice == "circle") {
        var cir = new Geometry.circle(context, p, 30, "red");
        cir.draw();
        container.push(cir);
        console.log(container);
    }
    else if (choice == "triangle") {
        var tri = new Geometry.polygon(context, p.x, p.y, 3, "green");
        tri.draw();
        container1.push(tri);
        console.log(container1);
    }
    else if (choice == "square") {
        var sq = new Geometry.polygon(context, p.x, p.y, 4, "blue");
        sq.draw();
        container1.push(sq);
        console.log(container1);
    }
    else if (choice == "pentagon") {
        var pent = new Geometry.polygon(context, p.x, p.y, 5, "yellow");
        pent.draw();
        container1.push(pent);
        console.log(container1);
    }
    else if (choice == "change") {
        for (var i = 0; i < container.length; i++) {
            if (container[i].isinside(p)) {
                container[i].color = colorcontainer[random(0, 6)];
            }
        }
        for (var i = 0; i < container1.length; i++) {
            if (container1[i].isinside(p)) {
                container1[i].color = colorcontainer[random(0, 6)];
            }
        }
        drawagain();
    }
}
function drawagain() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    for (var i = 0; i < container.length; i++) {
        container[i].draw();
    }
    for (var i = 0; i < container1.length; i++) {
        container1[i].draw();
    }
}
function random(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}
//# sourceMappingURL=app.js.map