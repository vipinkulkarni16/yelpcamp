var Geometry;
(function (Geometry) {
    class point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    Geometry.point = point;
    class circle {
        constructor(context, pt1, r) {
            this.vx = 5;
            this.s = 1;
            this.pt1 = pt1;
            this.context = context;
            this.r = r;
            this.fix = new point(pt1.x, pt1.y);
        }
        // drawing circle
        draw() {
            this.context.beginPath();
            this.context.arc(this.pt1.x, this.pt1.y, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 0.4;
            this.context.fillStyle = "rgb(114, 206, 230)"; //this.color;
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
        }
        update(canvas) {
            console.log("update");
            this.pt1.x = this.pt1.x + this.random(0, 8);
            this.pt1 = new point(this.pt1.x, this.pt1.y);
            console.log(this.pt1.x);
            this.check();
        }
        check() {
            if (this.pt1.x > canvas.width) {
                this.pt1.x = 0;
            }
            else if (this.pt1.x < this.r) {
                this.s = 1;
            }
        }
        random(min, max) {
            return Math.round(Math.random() * (max - min) + min);
        }
    }
    Geometry.circle = circle;
    class line {
        constructor(context, point1, point2, color) {
            this.pt1 = point1;
            this.pt2 = point2;
            this.context = context;
            this.color = color;
            this._ang = Math.atan2(this.pt2.y - this.pt1.y, this.pt2.x - this.pt1.x);
        }
        draw() {
            console.log("wrk");
            this.context.beginPath();
            this.context.moveTo(this.pt1.x, this.pt1.y);
            this.context.lineTo(this.pt2.x, this.pt2.y);
            this.context.lineWidth = 2;
            this.context.strokeStyle = this.color;
            this.context.stroke();
        }
    }
    Geometry.line = line;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map