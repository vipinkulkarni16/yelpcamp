class table // creating a simple table class
{
    protected _table;
    public noofrows=0;
    public noofcolunm=0;
    private type;
    private id;
    protected value;
    //constructor of the class....
    constructor(table:HTMLTableElement,noofrows:number,noofcolunm:number,celldatatype:string,id:string,value:string){

        this._table = table;
        this.noofrows = noofrows;
        this.noofcolunm = noofcolunm;
        this.type = celldatatype;
        this.id = id;
        this.value=value;

    }
    //drawing the table....
    create() 
    {
        while(this._table.rows.length>0)    //clearing preformed rows...
        {
            this._table.deleteRow(0);
        }

        for(var i=0;i<this.noofrows;i++)
        {
            var row = this._table.insertRow();
            for(var j=0;j<this.noofcolunm;j++)
            {
                var cell = row.insertCell();
                var option:HTMLInputElement =<HTMLInputElement> document.createElement("input");
                option.value = this.value;
                option.type = this.type;
                option.id = this.id +i.toString() + j.toString();
                cell.appendChild(option);
                console.log(option.id);
            }
        }
    }

}
//              creating a dropdown                //
class dropdown 
{
    private tb1:HTMLTableElement;

    private list:HTMLSelectElement; 
    private list1:{value:number}[];
    private startValue:number;
    private endValue:number;

    constructor(selectId:HTMLSelectElement,valstart:number,valend:number)
    {

        this.list = selectId;
        this.list1 = [];
        this.startValue = valstart;
        this.endValue = valend;
        

    }
    
    create(){
        for(let i = this.startValue; i <= this.endValue; i++)    
    {
        this.list1.push({

            value: i
        
        })
    }


        //printing the list of items....
        for (var i = 0 ; i < this.list1.length ; i++) {

            let option:HTMLOptionElement =<HTMLOptionElement> document.createElement("option");
            option.value = this.list1[i].value.toString();
            option.text = "    " + this.list1[i].value + "    ";
            this.list.appendChild(option);
        }

    }

}

class result 
{
    public list;
    protected selected_option:number;
    public angle:number = 0;
    public sum_x = 0;
    public sum_y = 0;
    public result_val = 0;
    
    constructor(dropdownID:HTMLSelectElement)
    {
        this.selected_option = parseFloat(dropdownID.value);
        this.list = [];
        
    
    }

    get line():[]
    {
        return this.list;
    }

    display(z:HTMLParagraphElement)
    {
        for (var i = 0; i < this.selected_option; i++) {
            
            var a:HTMLInputElement =<HTMLInputElement> document.getElementById("t" + i.toString() + "0");
            var b:HTMLInputElement =<HTMLInputElement> document.getElementById("t" +  i.toString() + "1");
            var x = parseFloat(a.value);
            var y = parseFloat(b.value);
            console.log(a);
            console.log(b);
            
            var x1 = x * Math.cos(Math.PI / 180 * y);
            var y1 =x * Math.sin(Math.PI / 180 * y);
            this.sum_x = this.sum_x + x1;
            this.sum_y = this.sum_y + y1;
            
            this.list.push({
                x:x1,
                y:y1,
            })
            
            console.log(this.sum_x);
            console.log(this.sum_y);
            console.log(this.line);
        
        }
        this.result_val = Math.sqrt(Math.pow(this.sum_x, 2) + Math.pow(this.sum_y, 2));
        
        this.angle = Math.atan2(this.sum_y , this.sum_x)*180/Math.PI;

        z.innerHTML = "resultant =" + this.result_val + "<br/>" + "At an angle of " + this.angle;
    }
}
    
class drawing 
{
    protected context;
    public line:{x:number;y:number}[];
    constructor(context:CanvasRenderingContext2D,line:[])
    {
        
        this.context = context;
        this.line =line;
    }
    
    draw()
    {
        
        var last = this.line.length;
        console.log(last);
        //drawing the force vectors.....
        this.context.translate(200, 500);
        this.context.scale(1, -1);
        this.context.beginPath();
        this.context.moveTo(0, 0);
        console.log(" wrk 1");
        for (var i = 0; i < last; i++) {

            this.context.lineTo(this.line[i].x, this.line[i].y);
            console.log(this.line[i].x);
            console.log(this.line[i].y);

        }
        this.context.lineWidth = 2;
        this.context.strokeStyle = "black";
        this.context.stroke();
        var l = (last-1)
        //drawing the resultant.....
        this.context.beginPath();
        this.context.moveTo(0, 0);
        this.context.lineTo(this.line[l].x, this.line[l].y);
        this.context.lineWidth = 3;
        this.context.strokeStyle = "red";
        this.context.stroke();
        
        
    }

}
