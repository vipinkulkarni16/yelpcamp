namespace Geometry
{
    export class point
    {
        public x:number;
        public y:number;
        
        constructor(x:number,y:number)
        {
            this.x = x;
            this.y = y;
        }
    }

    export class closePath {
        
        public vx:number;
        public vy:number;
        
        constructor(x:number,y:number){
            this.vx = x;
            this.vy =y;
        }

        
    }

    
    export class circle { //declaring a class circle

        public pt1:point;
        public context:CanvasRenderingContext2D;
        public r:number;
        public _color: string;
        private ang:number =0;


        constructor(context:CanvasRenderingContext2D,pt1:point,r:number,color:string){

            this.pt1 = pt1;
            this.context = context;
            this.r=r;
            this._color=color;
            
        }

        draw(){
            this.context.beginPath();
            this.context.arc(this.pt1.x + 200, this.pt1.y + 300, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 2;
            this.context.fillStyle = this._color;
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
            this.update();
        }

        update(){
            this.ang++
            var x =this.ang;
            var y =100 * Math.sin(-this.ang*Math.PI/180);
            this.pt1 = new point(x,y);
            console.log(x,y);
            if(this.ang==360){
                this.ang = 1;
            }
        }
    }

    export class sin{

        public pt1:point;
        public pt2:number;
        public context:CanvasRenderingContext2D;
        public r:number;
        public _color: string;
        private ang:number =0;

        constructor(context:CanvasRenderingContext2D,pt1:point,color:string)
        {
            this.pt1 = pt1;
            this.context = context;
            this._color = color;
        }

        draw(){
            this.context.save();
            this.context.translate(this.pt1.x,this.pt1.y);
            this.context.scale(1,-1);
            this.context.beginPath();
            this.context.moveTo(0,0)
            for(let i=0;i<=360;i++){
                this.context.lineTo(i,100*Math.sin(i*Math.PI/180));
            }
            this.context.lineWidth = 2;
            this.context.strokeStyle = this._color;
            this.context.stroke();

            this.context.beginPath();
            this.context.moveTo(0,100);   
            this.context.lineTo(0,-100);
            this.context.moveTo(0,0);   
            this.context.lineTo( 400,0);            
            this.context.lineWidth = 2;
            this.context.strokeStyle = "black";
            this.context.stroke();
            this.context.restore();
            
        }

    }
}