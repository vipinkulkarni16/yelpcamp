namespace Geometry{
    export class point
        {
            public x:number;
            public y:number;
            
            constructor(x:number,y:number)
            {
                this.x = x;
                this.y = y;
            }
        }
        
    export class circle { //declaring a class circle

        public pt1:point;
        public context:CanvasRenderingContext2D;
        public r:number;
        public color: string;
        private cp:point;
        private _color:string;
        private vx:number =5;
        public fix:point;
        public s:number=1;

        constructor(context:CanvasRenderingContext2D,pt1:point,r:number){

            this.pt1 = pt1;
            this.context = context;
            this.r=r;
            this.fix = new point(pt1.x,pt1.y);
        }
        // drawing circle
        draw(){

            this.context.beginPath();
            this.context.arc(this.pt1.x, this.pt1.y, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 0.4;
            this.context.fillStyle = "rgb(114, 206, 230)"; //this.color;
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
        }
        
        update(canvas:HTMLCanvasElement){
            console.log("update");
            this.pt1.x = this.pt1.x + this.random(0,8);
            this.pt1 = new point(this.pt1.x,this.pt1.y);
            console.log(this.pt1.x);
            this.check();
        }

        private check(){
            
            if(this.pt1.x > canvas.width)
            {
                this.pt1.x = 0 ;
            }else if(this.pt1.x<this.r){
                this.s = 1 ;
            }
            
        }
            
        random(min:number,max:number){
            return Math.round(Math.random() * (max - min) + min);
        }
    }

    export class line {

        public pt1:point;
        public pt2:point;
        public context:CanvasRenderingContext2D;
        private ang:number;
        public color:string;
        private _ang:number;

        constructor(context:CanvasRenderingContext2D,point1:point,point2:point,color:string){

            this.pt1 = point1;
            this.pt2 = point2;
            this.context = context;
            this.color=color;
            this._ang = Math.atan2(this.pt2.y-this.pt1.y,this.pt2.x-this.pt1.x);

        }

        draw()
        {
            console.log("wrk");
            this.context.beginPath();
            this.context.moveTo(this.pt1.x, this.pt1.y);
            this.context.lineTo(this.pt2.x, this.pt2.y);
            this.context.lineWidth = 2;
            this.context.strokeStyle = this.color;
            this.context.stroke();
            
        }

    }
}
