class table // creating a simple table class
{
    protected _table;
    public noofrows=0;
    public noofcolunm=0;
    private type;
    private id;
    protected value;
    //constructor of the class....
    constructor(table:HTMLTableElement,noofrows:number,noofcolunm:number,celldatatype:string,id:string,value:string){

        this._table = table;
        this.noofrows = noofrows;
        this.noofcolunm = noofcolunm;
        this.type = celldatatype;
        this.id = id;
        this.value=value;

    }
    //drawing the table....
    create() 
    {
        while(this._table.rows.length>0)    //clearing preformed rows...
        {
            this._table.deleteRow(0);
        }

        for(var i=0;i<this.noofrows;i++)
        {
            var row = this._table.insertRow();
            for(var j=0;j<this.noofcolunm;j++)
            {
                var cell = row.insertCell();
                var option:HTMLInputElement =<HTMLInputElement> document.createElement("input");
                option.value = this.value;
                option.type = this.type;
                option.id = this.id +i.toString() + j.toString();
                cell.appendChild(option);
                console.log(option.id);
            }
        }
    }

}
//              creating a dropdown                //
class dropdown 
{
    private tb1:HTMLTableElement;

    private list:HTMLSelectElement; 
    private list1:{value:number}[];
    private startValue:number;
    private endValue:number;

    constructor(selectId:HTMLSelectElement,valstart:number,valend:number)
    {

        this.list = selectId;
        this.list1 = [];
        this.startValue = valstart;
        this.endValue = valend;
        

    }
    
    create(){
        for(let i = this.startValue; i <= this.endValue; i++)    
    {
        this.list1.push({

            value: i
        
        })
    }


        //printing the list of items....
        for (var i = 0 ; i < this.list1.length ; i++) {

            let option:HTMLOptionElement =<HTMLOptionElement> document.createElement("option");
            option.value = this.list1[i].value.toString();
            option.text = "    " + this.list1[i].value + "    ";
            this.list.appendChild(option);
        }

    }
}

//creating drop down on web page....
var select:HTMLSelectElement =<HTMLSelectElement> document.getElementById("drop_down");
var drop = new dropdown(select,2,6);
drop.create();

//onchange function.....
function ajink(){

    var t1:HTMLTableElement = <HTMLTableElement> document.getElementById("a1");

    var a:table = new table(t1,parseFloat(select.value),2,"input","t","");
    a.create();

}

//result function....
    function result_f(z:HTMLInputElement) {

        var a:HTMLInputElement =<HTMLInputElement> document.getElementById("drop_down");
        var selected_option = parseFloat(a.value);
        
        var sum_x = 0;
        var sum_y = 0;
    
        for (var i = 0; i < selected_option; i++) {
            var a:HTMLInputElement =<HTMLInputElement> document.getElementById("t" + i.toString() + "0");
            var b:HTMLInputElement =<HTMLInputElement> document.getElementById("t" +  i.toString() + "1");
            var x = parseFloat(a.value);
            var y = parseFloat(b.value);
            console.log(a);
            console.log(b);
            
            sum_x = sum_x + x * Math.cos(Math.PI / 180 * y);
            sum_y = sum_y + x * Math.sin(Math.PI / 180 * y);
            
            console.log(sum_x);
            console.log(sum_y);
        
        }
        var result = Math.sqrt(Math.pow(sum_x, 2) + Math.pow(sum_y, 2));
        
        var angle = Math.atan2(sum_y , sum_x)*180/Math.PI;

        z.innerHTML = "resultant =" + result + "<br/>" + "At an angle of " + angle;
    }
