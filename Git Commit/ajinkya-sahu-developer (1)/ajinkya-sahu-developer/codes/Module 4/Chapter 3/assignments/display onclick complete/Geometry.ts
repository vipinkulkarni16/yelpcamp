namespace Geometry 
{
    export class points 
    {
        private _x:number;
        private _y:number;

        constructor(x_coordinate:number,y_coordinate:number)
        {
            this._x = x_coordinate;
            this._y = y_coordinate;
        }

        get x():number
        {
            return this._x;
        }
        get y():number
        {
            return this._y;
        }
    }
}