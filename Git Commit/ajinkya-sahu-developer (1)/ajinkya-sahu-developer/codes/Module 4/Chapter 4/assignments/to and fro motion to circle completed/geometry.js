var Geometry;
(function (Geometry) {
    class point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    Geometry.point = point;
    class circle {
        constructor(context, pt1, r) {
            this.color = ["red", "green", "blue", "yellow", "white", "black", "orange", "pink"];
            this.vx = 2;
            this.s = 1;
            this.pt1 = pt1;
            this.context = context;
            this.r = r;
            this.fix = new point(pt1.x, pt1.y);
        }
        // drawing circle
        draw() {
            this.context.beginPath();
            this.context.arc(this.pt1.x, this.pt1.y, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 2;
            this.context.fillStyle = this.color[this.random(0, 7)];
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
        }
        update(canvas) {
            console.log("update");
            this.pt1.x = this.pt1.x + this.vx * this.s;
            this.pt1 = new point(this.pt1.x, this.pt1.y);
            console.log(this.pt1.x);
            this.check();
        }
        check() {
            if (this.pt1.x > canvas.width) {
                this.s = -1;
            }
            else if (this.pt1.x < this.r) {
                this.s = 1;
            }
        }
        random(min, max) {
            return Math.round(Math.random() * (max - min) + min);
        }
    }
    Geometry.circle = circle;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map