var Geometry;
(function (Geometry) {
    class point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    Geometry.point = point;
    class circle {
        constructor(context, pt1, r) {
            this.color = ["red", "green", "blue", "yellow", "white", "black", "orange", "pink"];
            this.s = 1;
            this.ang = 45;
            this.up = true;
            this.pt1 = pt1;
            this.context = context;
            this.r = r;
            this.vx = this.pt1.x;
        }
        // drawing circle
        draw() {
            this.context.beginPath();
            this.context.arc(this.pt1.x, this.pt1.y, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 2;
            this.context.fillStyle = this.color[this.random(0, 7)];
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
        }
        update(length) {
            this.length = length;
            console.log("update");
            this.pt1.x = this.pt1.x + Math.cos(this.ang * Math.PI / 180) * this.s;
            this.pt1.y = this.pt1.y + Math.sin(this.ang * Math.PI / 180) * this.s;
            this.pt1 = new point(this.pt1.x, this.pt1.y);
            console.log(this.pt1.x);
            console.log(this.s, this.vx);
            this.check();
        }
        check() {
            if (this.vx + Math.cos(this.ang * Math.PI / 180) * this.length <= this.pt1.x) {
                this.s--;
            }
            else if (this.vx - Math.cos(this.ang * Math.PI / 180) * this.length > this.pt1.x) {
                this.s++;
            }
        }
        random(min, max) {
            return Math.round(Math.random() * (max - min) + min);
        }
    }
    Geometry.circle = circle;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map