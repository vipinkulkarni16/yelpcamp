var Geometry;
(function (Geometry) {
    class point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    Geometry.point = point;
    class circle {
        constructor(context, pt1, r, color, pt2) {
            this.pt1 = pt1;
            this.pt2 = pt2;
            this.context = context;
            this.ang = 0;
            this.r = r;
            this.color = color;
        }
        draw() {
            this.context.beginPath();
            this.context.arc(this.pt1.x, this.pt1.y, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 2;
            this.context.fillStyle = this.color;
            this.context.fill();
            //this.context.strokeStyle = "black";
            this.context.stroke();
        }
    }
    Geometry.circle = circle;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=Geometry.js.map