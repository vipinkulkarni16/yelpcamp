class point
    {
        public x:number;
        public y:number;
        
        constructor(x:number,y:number)
        {
            this.x = x;
            this.y = y;
        }
    }
    
class circle { //declaring a class circle

        public pt1:point;
        public context:CanvasRenderingContext2D;
        public r:number;
        public color: string="red";
        private cp:point;
        private _color:string;
        public connected:boolean=false;
        constructor(context:CanvasRenderingContext2D,pt1:point,r:number){

            this.pt1 = pt1;
            this.context = context;
            this.r=r;
        }
        // drawing circle
        draw(){

            this.context.beginPath();
            this.context.arc(this.pt1.x, this.pt1.y, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 2;
            this.context.fillStyle = this.color;
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
            if(this.connected){
                this.connection();
            }
        }
        isinside(pt:point): boolean{
            let rad =Math.sqrt(Math.pow(pt.x-this.pt1.x,2)+Math.pow(pt.y-this.pt1.y,2));
            if(rad<this.r){
                return (true);
            }
            else{
                return(false);
            }
        }
        setconnection(pt2:point){
            this.cp=pt2;
            this.connected=true;
        }
        connection(){
            this.context.beginPath();
            this.context.moveTo(this.pt1.x, this.pt1.y);
            this.context.lineTo(this.cp.x, this.cp.y);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "blue";
            this.context.stroke();
        }
        get centerpt(){
            return(this.pt1);
        }
    }
