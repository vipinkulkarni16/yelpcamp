namespace geometry {
    export class point {

        public x:number;
        public y:number;

        constructor(x:number,y:number){
            this.x=x;
            this.y=y;
        }
    }

    export class bird{

        private pic;
        private _w :number;
        private _stpt:point;
        public context:CanvasRenderingContext2D;
        private s:number;
        private _stx:number;


        constructor(context:CanvasRenderingContext2D,pic:HTMLImageElement,stpt:point){
            this.pic = pic;
            this._stpt=stpt;
            this.context=context;
            this.s=1; 
            this._stx=this._stpt.x;   
        }

    
        draw(){
            
            this.context.drawImage(this.pic,this._stpt.x,this._stpt.y,50,50);

        }

        tofromotion(tofrolength:number) {

            this._stpt.x+=this.s;
            if (this._stx + tofrolength <= this._stpt.x){
                this.s= -1;
            }else if (this._stx-tofrolength > this._stpt.x){
                this.s = 1;
            }
        }
    }
}


