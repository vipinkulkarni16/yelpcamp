var canvas:HTMLCanvasElement =<HTMLCanvasElement> document.getElementById("mycanvas");
var context:CanvasRenderingContext2D =<CanvasRenderingContext2D> canvas.getContext("2d");

canvas.addEventListener("click",click,false);
var rect = canvas.getBoundingClientRect();


var s1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
var choice:string;

function circle1() 
{
    var s=parseFloat(s1.value);
    if( s>0 && isNaN(s)!=true ){
        choice="circle";
    }
    else{
        alert("Entered no is not greater than 0 or it is not a number")
    }
}

function connect() 
{
    choice="connect";
}

var cc:{circle:circle}[]=[];
var start1=0;
var storei;

function click(e:MouseEvent)
{
    if(choice=="circle")
    {
        var s=parseFloat(s1.value)
        var c1 = new point(e.clientX-rect.x,e.clientY-rect.y);
        var cir = new circle(context,c1, s);
        cir.draw();
        cc.push({circle:cir});

    }else if(choice=="connect")
    {
        for (let i = 0; i < cc.length; i++) {
            if(cc[i].circle.isinside(new point(e.clientX-rect.x,e.clientY-rect.y))){
                if ( start1 == 0) {
                    storei = i;
                    start1 = 1;
                    break;
                } else if ( start1 == 1) {
                    cc[storei].circle.setconnection(cc[i].circle.centerpt);
                    storei = -1;
                    start1 = 0;
                    break;
                }
            }
        }
        for (let i = 0; i < cc.length; i++) {
            cc[i].circle.draw();
        }
    }

}