var canvas: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context: CanvasRenderingContext2D = canvas.getContext("2d");


context.translate(500,200);
context.scale(1,-1);
context.save();

draw();


function draw(){

    context.beginPath();
    for(var i=1;i<=7;i++){
        context.lineTo(100,0);
        context.rotate(Math.PI/3);
    }
    context.fillStyle="red";
    context.lineWidth = 2;
    context.fill();
    context.stroke();

}