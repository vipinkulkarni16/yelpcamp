var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
canvas.addEventListener("click", click);
var choice;
function circle() {
    choice = "circle";
}
function triangle() {
    choice = "triangle";
}
function square() {
    choice = "square";
}
function pentagon() {
    choice = "pentagon";
}
function hexagon() {
    choice = "hexagon";
}
function octagon() {
    choice = "octagon";
}
function click(e) {
    var p = new Geometry.point(e.clientX, e.clientY);
    console.log(e.clientX, e.clientY);
    if (choice == "circle") {
        var cir = new Geometry.circle(context, p.x, p.y, 30, "red");
        cir.draw();
    }
    else if (choice == "triangle") {
        var tri = new Geometry.polygon(context, p.x, p.y, 3, "green");
        tri.draw();
    }
    else if (choice == "square") {
        var sq = new Geometry.polygon(context, p.x, p.y, 4, "blue");
        sq.draw();
    }
    else if (choice == "pentagon") {
        var pent = new Geometry.polygon(context, p.x, p.y, 5, "yellow");
        pent.draw();
    }
    else if (choice == "hexagon") {
        var hex = new Geometry.polygon(context, p.x, p.y, 6, "pink");
        hex.draw();
    }
    else if (choice == "octagon") {
        var oct = new Geometry.polygon(context, p.x, p.y, 8, "orange");
        oct.draw();
    }
}
//# sourceMappingURL=app.js.map