var canvas:HTMLCanvasElement =<HTMLCanvasElement> document.getElementById("mycanvas");
var context:CanvasRenderingContext2D =<CanvasRenderingContext2D> canvas.getContext("2d");

//context.translate(0,600);
//context.scale(-1,1);
// context.save();
//context.rotate(Math.PI);

canvas.addEventListener("click",add);

function add(e:MouseEvent){

    console.log(e.clientX,e.clientY);
    var p1 = new Geometry.point(e.clientX, e.clientY);
    var cir = new Geometry.circle(context,p1,50,"white");
    var x1= e.clientX + 50 * Math.cos(2.5/4 *Math.PI);
    var x2= e.clientX + 50 * Math.cos(85/180*Math.PI);
    var y1= e.clientY + 50 * Math.sin(2.5/4*Math.PI);
    var y2= e.clientY + 50 * Math.sin(85/180*Math.PI);
    var p2 = new Geometry.point(x1,y1);
    var p3 = new Geometry.point(x2,y2);
    var p4 = new Geometry.point(x1,y1+90);
    var p5 = new Geometry.point(x2,y2+90);
    var l1 = new Geometry.line(context,p2,p4,"black");
    var l2 = new Geometry.line(context,p3,p5,"black");
    var l3 = new Geometry.line(context,p4,p5,"black");
    console.log(p1,p2,p3,p3,p4,p5);
    cir.draw();
    l1.draw();
    l2.draw();
    l3.draw();

}
