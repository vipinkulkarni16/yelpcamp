var Geometry;
(function (Geometry) {
    class point {
        constructor(x_coordinate, y_coordinate) {
            this._x = x_coordinate;
            this._y = y_coordinate;
        }
        get x() {
            return this._x;
        }
        get y() {
            return this._y;
        }
    }
    Geometry.point = point;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=Geometry.js.map