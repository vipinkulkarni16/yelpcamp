class point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}
class circle {
    constructor(context, pt1, r) {
        this.color = "red";
        this.connected = false;
        this.pt1 = pt1;
        this.context = context;
        this.r = r;
    }
    // drawing circle
    draw() {
        this.context.beginPath();
        this.context.arc(this.pt1.x, this.pt1.y, this.r, 0, Math.PI * 2);
        this.context.lineWidth = 2;
        this.context.fillStyle = this.color;
        this.context.fill();
        this.context.strokeStyle = "black";
        this.context.stroke();
        if (this.connected) {
            this.connection();
        }
    }
    isinside(pt) {
        let rad = Math.sqrt(Math.pow(pt.x - this.pt1.x, 2) + Math.pow(pt.y - this.pt1.y, 2));
        if (rad < this.r) {
            return (true);
        }
        else {
            return (false);
        }
    }
    setconnection(pt2) {
        this.cp = pt2;
        this.connected = true;
    }
    connection() {
        this.context.beginPath();
        this.context.moveTo(this.pt1.x, this.pt1.y);
        this.context.lineTo(this.cp.x, this.cp.y);
        this.context.lineWidth = 4;
        this.context.strokeStyle = "blue";
        this.context.stroke();
    }
    get centerpt() {
        return (this.pt1);
    }
}
//# sourceMappingURL=geometry.js.map