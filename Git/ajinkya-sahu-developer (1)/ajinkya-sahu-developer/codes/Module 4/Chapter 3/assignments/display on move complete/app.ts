var canvas:HTMLCanvasElement = <HTMLCanvasElement> document.getElementById("mycanvas");
var context:CanvasRenderingContext2D=<CanvasRenderingContext2D> canvas.getContext("2d");

let t1:HTMLInputElement = <HTMLInputElement> document.getElementById("t1");


canvas.addEventListener("mousemove",mousemove,false);

function mousemove (e:MouseEvent)
{
    var rect = canvas.getBoundingClientRect();
    let p1 = new Geometry.point(e.clientX-rect.x,e.clientY-rect.y);
    t1.value = p1.x + " , " + p1.y;

}
