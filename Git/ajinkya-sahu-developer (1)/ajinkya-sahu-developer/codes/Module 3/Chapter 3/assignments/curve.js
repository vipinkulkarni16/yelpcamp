var data = [];
var sin = [];
var cos = [];
for (i = -2 * Math.PI; i <= Math.PI * 2; i += 0.1) {
    sin.push({
        x: i,
        y: Math.sin(i),
    });
    cos.push({
        x: i,
        y: Math.cos(i),
    });
}
drawgraph("g1", sin, "x value", "y value");
drawgraph("g2", cos, "x value", "y value");
data.push({
    type: "spline",
    xValueType: "float",
    showInLegend: false,
    markerSize: 2,
    name: "f(x1)",
    dataPoints: sin
});
data.push({
    type: "spline",
    xValueType: "float",
    showInLegend: false,
    markerSize: 2,
    name: "f(x2)",
    dataPoints: cos
});
graphline("g3", data, "", "", 200, -200);
//graphline("g2",datapoint2,"","",200,-200);
//drawgraph("g1",datapoints3,"x values","y value");
//drawgraph("g2",datapoints4,"x values","y value");
//# sourceMappingURL=curve.js.map