var Geometry;
(function (Geometry) {
    class point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    Geometry.point = point;
    class line {
        constructor(context, point1, point2, color) {
            this.pt1 = point1;
            this.pt2 = point2;
            this.context = context;
            this.color = color;
            this._ang = Math.atan2(this.pt2.y - this.pt1.y, this.pt2.x - this.pt1.x);
        }
        draw() {
            console.log("wrk");
            this.context.beginPath();
            this.context.moveTo(this.pt1.x, this.pt1.y);
            this.context.lineTo(this.pt2.x, this.pt2.y);
            this.context.lineWidth = 2;
            this.context.strokeStyle = this.color;
            this.context.stroke();
            this.update();
        }
        update() {
            this._ang++;
            console.log(this._ang);
            var len = Math.sqrt(Math.pow(this.pt1.x - this.pt2.x, 2) + Math.pow(this.pt1.y - this.pt2.y, 2));
            console.log(len);
            this.pt2.x = this.pt1.x + len * Math.cos(this._ang * Math.PI / 180);
            this.pt2.y = this.pt1.y + len * Math.sin(this._ang * Math.PI / 180);
            console.log(this.pt2.x, this.pt2.y);
            if (this._ang >= 360) {
                this.ang = 0;
            }
        }
    }
    Geometry.line = line;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=Geometry.js.map