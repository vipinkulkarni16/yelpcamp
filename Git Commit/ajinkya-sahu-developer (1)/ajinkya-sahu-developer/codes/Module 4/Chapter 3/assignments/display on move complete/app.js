var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
let t1 = document.getElementById("t1");
context.translate(0, 600);
context.scale(1, -1);
canvas.addEventListener("mousemove", mousemove, false);
function mousemove(e) {
    var rect = canvas.getBoundingClientRect();
    let p1 = new Geometry.point(e.clientX - rect.x, 600 - e.clientY - rect.y);
    t1.value = p1.x + " , " + p1.y;
}
//# sourceMappingURL=app.js.map