var table1 = document.getElementById("tb1");
var table2 = document.getElementById("tb2");
var table3 = document.getElementById("tb3");
var mata = document.getElementById("mat_a");
var matb = document.getElementById("mat_b");
var mat_type = [];
mat_type.push({
    value: 0,
    name: "1 * 1",
    m: "1",
    n: "1"
});
mat_type.push({
    value: 1,
    name: "1 * 2",
    m: "1",
    n: "2"
});
mat_type.push({
    value: 2,
    name: "2 * 2",
    m: "2",
    n: "2"
});
mat_type.push({
    value: 3,
    name: "2 * 3",
    m: "2",
    n: "3"
});
mat_type.push({
    value: 4,
    name: "3 * 2",
    m: "3",
    n: "2"
});
mat_type.push({
    value: 5,
    name: "3 * 3",
    m: "3",
    n: "3"
});
mat_type.push({
    value: 6,
    name: "3 * 4",
    m: "3",
    n: "4"
});
mat_type.push({
    value: 7,
    name: "4 * 3",
    m: "4",
    n: "3"
});
mat_type.push({
    value: 8,
    name: "4 * 4",
    m: "4",
    n: "4"
});
for (i = 0; i < 9; i++) {
    var x = document.createElement("option");
    x.text = mat_type[i].name;
    x.value = mat_type[i].value.toString();
    //x.m = mat_type[i].m;
    //x.n = mat_type[i].n;
    mata.add(x);
}
for (var i = 0; i < 9; i++) {
    var x = document.createElement("option");
    x.text = mat_type[i].name;
    x.value = mat_type[i].value.toString();
    //x.m = mat_type[i].m;
    //x.n = mat_type[i].n;
    matb.add(x);
}
function clear(a) {
    //clearing
    while (a.rows.length > 0) {
        a.deleteRow(0);
    }
}
function create(r, c, id, t1) {
    //clearing
    clear(t1);
    for (var i = 1; i <= r; i++) {
        var row = t1.insertRow();
        for (var j = 1; j <= c; j++) {
            var cell = row.insertCell();
            var t = document.createElement("input");
            t.type = "number";
            t.id = id + i + j;
            cell.appendChild(t);
            console.log(t.id);
        }
    }
}
function matrix_a() {
    var select_option = document.getElementById("mat_a");
    var selected_option = parseInt(select_option.value);
    var r;
    var c;
    for (var i = 0; i < 9; i++) {
        if (mat_type[i].value == selected_option) {
            r = mat_type[i].m;
            c = mat_type[i].n;
            console.log(r);
            console.log(c);
        }
    }
    create(r, c, "p", table1);
}
function matrix_b() {
    var select_option = document.getElementById("mat_b");
    var selected_option = parseInt(select_option.value);
    var r;
    var c;
    for (var i = 0; i < 9; i++) {
        if (mat_type[i].value == selected_option) {
            r = mat_type[i].m;
            c = mat_type[i].n;
            console.log(r);
            console.log(c);
        }
    }
    create(r, c, "q", table2);
}
function multiply() {
    var r = document.getElementById("mat_a");
    var mat1 = parseInt(r.value);
    var s = document.getElementById("mat_b");
    var mat2 = parseInt(s.value);
    var r1;
    var r2;
    var c1;
    var c2;
    for (var i = 0; i < 9; i++) {
        if (mat_type[i].value == mat1) {
            r1 = mat_type[i].m;
            c1 = mat_type[i].n;
            console.log(r1);
            console.log(c1);
        }
    }
    for (i = 0; i < 9; i++) {
        if (mat_type[i].value == mat2) {
            r2 = mat_type[i].m;
            c2 = mat_type[i].n;
            console.log(r2);
            console.log(c2);
        }
    }
    //var a:HTMLTableElement =<HTMLTableElement> document.getElementById("table3");
    clear(table3);
    if (c1 == r2) {
        for (i = 1; i <= parseInt(r1); i++) {
            var row = table3.insertRow();
            for (var j = 1; j <= parseInt(c2); j++) {
                var cell = row.insertCell();
                var t = document.createElement("input");
                t.type = "text";
                t.id = "r" + i + j;
                var z = 0;
                for (var k = 1; k <= parseInt(c1); k++) {
                    var x = document.getElementById("p" + i + k);
                    var x1 = parseInt(x.value);
                    var y = document.getElementById("q" + k + j);
                    var y1 = parseInt(y.value);
                    console.log(x1);
                    console.log(y1);
                    console.log(z);
                    z = z + y1 * x1;
                }
                t.value = z.toString();
                cell.appendChild(t);
            }
        }
    }
    else {
        alert("Choose the correct order of 2nd matrix." + " Matrix rule is no. of rows of matrix B should equal to coloumn of matrix A");
    }
}
//# sourceMappingURL=multiplication.js.map