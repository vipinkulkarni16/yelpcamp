namespace Geometry {

    export class line {

        public pt1x:number;
        public pt1y:number;
        public pt2x:number;
        public pt2y:number;
        public context:CanvasRenderingContext2D;
        private ang:number;
        public color:string;
        private up:boolean;
        private down:boolean;

        constructor(context:CanvasRenderingContext2D,pt1x:number,pt1y:number,pt2x:number,pt2y:number,color:string){

            this.pt1x = pt1x;
            this.pt2x = pt2x;
            this.pt1y = pt1y;
            this.pt2y = pt2y;
            this.context = context;
            this.ang = -20;
            this.color=color;
            this.up = true;
            this.down = false;

        }

        draw(){

        this.context.beginPath();
        this.context.moveTo(this.pt1x, this.pt1y);
        this.context.lineTo(this.pt2x, this.pt2y);
        this.context.lineWidth = 2;
        this.context.strokeStyle = this.color;
        this.context.stroke();

        }
    }
}