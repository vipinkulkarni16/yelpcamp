declare var drawgraph;
declare var drawgraph2;
declare var drawlogx;
declare var graphline;
declare var graphlogx;

var datapoints1:{x:number,y:number}[] = [];
var datapoints2:{x:number,y:number}[] = [];
for ( var i = 1; i <= 10; i++) {
    datapoints1.push({
        x: i,
        y: Math.pow(i,2),
    });
    datapoints2.push({
        x: i,
        y: Math.pow(i,3),
    });
}        
drawgraph("g1", datapoints1, "x values", "x");

drawgraph2("g2", datapoints1, datapoints2, "x-axis", "y-axis", "comparison", "x^2", "x^3");

graphline("g3",datapoints1,"X-value","square");



//log graph
var logx:{x:number,y:number}[] = [];

for ( var i = 10;i <= 80; i+=10) {

    logx.push({
        y: i,
        x: i*4135,
    });

}

graphlogx("g4",logx,"x-axis","y-axis");



