namespace Geometry{
    export class point
        {
            public x:number;
            public y:number;
            
            constructor(x:number,y:number)
            {
                this.x = x;
                this.y = y;
            }
        }
        
    export class circle { //declaring a class circle

        public pt1:point;
        public context:CanvasRenderingContext2D;
        public r:number;
        public color: string[] = ["red","green","blue","yellow","white","black","orange","pink"];
        private cp:point;
        private _color:string;
        private vx:number;
        public s:number=1;
        private ang:number=45;
        private length:number;
        private up:boolean=true;
        constructor(context:CanvasRenderingContext2D,pt1:point,r:number){

            this.pt1 = pt1;
            this.context = context;
            this.r=r;
            this.vx = this.pt1.x;
            
        }
        // drawing circle
        draw(){

            this.context.beginPath();
            this.context.arc(this.pt1.x, this.pt1.y, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 2;
            this.context.fillStyle = this.color[this.random(0,7)];
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
        }
        
        update(length:number){
            this.length = length;
            console.log("update");
            this.pt1.x = this.pt1.x +  Math.cos(this.ang * Math.PI /180)*this.s;
            this.pt1.y = this.pt1.y +  Math.sin(this.ang * Math.PI /180)*this.s;
            this.pt1 = new point(this.pt1.x,this.pt1.y);
            console.log(this.pt1.x);
            console.log(this.s,this.vx);
            this.check();
        }

        private check(){
            if(this.vx+Math.cos(this.ang * Math.PI /180)*this.length<=this.pt1.x)
            {
                this.s--;
        }else if(this.vx-Math.cos(this.ang * Math.PI /180)*this.length>this.pt1.x){
               this.s++;
            }
        }
            
        random(min:number,max:number){
            return Math.round(Math.random() * (max - min) + min);
        }
    }
}
