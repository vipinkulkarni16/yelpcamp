namespace Geometry{
    export class point {

        public x:number;
        public y:number;

        constructor(x:number,y:number){
            this.x=x;
            this.y=y;
        }
    }

    export class circle{
        private ang:number;
        public pt1:point;
        public pt2:point;
        public context:CanvasRenderingContext2D;
        public r:number;
        public color: string;

        constructor(context:CanvasRenderingContext2D,pt1:point,r:number,color:string,pt2?:point)
        {

            this.pt1 = pt1;
            this.pt2 = pt2;
            this.context = context;
            this.ang =0;
            this.r=r;
            this.color=color;
            
        }

        draw(){

            this.context.beginPath();
            this.context.arc(this.pt1.x, this.pt1.y, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 2;
            this.context.fillStyle = this.color;
            this.context.fill();
            //this.context.strokeStyle = "black";
            this.context.stroke();
        
        }
    }
}