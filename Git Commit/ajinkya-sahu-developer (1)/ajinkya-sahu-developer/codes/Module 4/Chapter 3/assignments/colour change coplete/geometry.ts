namespace Geometry
{
    export class point
    {
        public x:number;
        public y:number;
        
        constructor(x:number,y:number)
        {
            this.x = x;
            this.y = y;
        }
    }

    export class closePath {
        
        public vx:number;
        public vy:number;
        
        constructor(x:number,y:number){
            this.vx = x;
            this.vy =y;
        }

        
    }

    
    export class circle { //declaring a class circle

        public pt1:point;
        public pt2:number;
        public context:CanvasRenderingContext2D;
        public r:number;
        public _color: string;


        constructor(context:CanvasRenderingContext2D,pt1:point,r:number,color:string){

            this.pt1 = pt1;
            this.context = context;
            this.r=r;
            this._color=color;
            
        }

        set color(colorString:string)
        {
            this._color =colorString;
        }

        get color():string
        {
            return this._color
        }
        // drawing circle
        draw(){

            this.context.beginPath();
            this.context.arc(this.pt1.x, this.pt1.y, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 2;
            this.context.fillStyle = this._color;
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
        
        }

        isinside(pt2:point){
            var r = Math.sqrt(Math.pow(pt2.x-this.pt1.x,2)-Math.pow(pt2.y-this.pt1.y,2));
            if(r<this.r){
                return true
            }else{
                return false
            }
        }

    }

    export class polygon 
    {
        public pt1:number;
        public pt2:number;
        public context:CanvasRenderingContext2D;
        public r:number;
        public color: string;


        constructor(context:CanvasRenderingContext2D,x:number,y:number,sides:number,color:string){

            this.pt1 = x;
            this.pt2 = y;
            this.context = context;
            this.r=sides;
            this.color=color;
            
        }
        // drawing polygon
        draw(){

            this.context.beginPath();
            this.context.moveTo(this.pt1 + 50,this.pt2);
            //console.log("wrk 1");
            for (var i=1; i<=this.r;i++)
            {
                var x1 = this.pt1 +50 * Math.cos((360/this.r) * i *Math.PI/180 );  
                var y1 = this.pt2 +50 * Math.sin(360/this.r * Math.PI/180 * i);                  
                this.context.lineTo(x1,y1);
                console.log(x1,y1);
            }
            this.context.lineTo(this.pt1 + 50,this.pt2);
            //console.log("wrk 2");
            this.context.closePath();
            this.context.lineWidth = 2;
            this.context.fillStyle = this.color;
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
            //console.log("wrk 3");
        
        }
        //checking for the points position...
        isinside(pt2:point){
            var r = Math.sqrt(Math.pow(pt2.x-this.pt1,2)-Math.pow(pt2.y-this.pt2,2));
            if( r < 50){
                return true
            }else{
                return false
            }
        }

    }

}
