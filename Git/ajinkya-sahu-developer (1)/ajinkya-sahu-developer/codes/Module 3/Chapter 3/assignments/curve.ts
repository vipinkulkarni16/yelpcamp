declare var graphline;
declare var drawgraph;

var data =[];
var sin = [];
var cos = [];
for (i = -2*Math.PI; i <=Math.PI * 2; i += 0.1) {
    sin.push({
        x: i,
        y: Math.sin(i),
    })
    cos.push({
        x: i,
        y: Math.cos(i),
    })
}
drawgraph("g1", sin, "x value", "y value");
drawgraph("g2", cos, "x value","y value");

