var canvas:HTMLCanvasElement =<HTMLCanvasElement> document.getElementById("mycanvas");
var context:CanvasRenderingContext2D =<CanvasRenderingContext2D> canvas.getContext("2d");

canvas.addEventListener("click",circle);

function circle(e:MouseEvent){

    var p = new Geometry.point(e.clientX,e.clientY);
    var cir = new Geometry.circle(context,p,30,"red");
    cir.draw();

}