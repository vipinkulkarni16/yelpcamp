var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
context.translate(0, 600);
context.scale(1, -1);
context.save();
//drawing axes
var l1 = new Geometry.line(context, 100, 250, 500, 250, "red");
var l2 = new Geometry.line(context, 550, 250, 900, 250, "red");
var l3 = new Geometry.line(context, 100, 100, 100, 400, "red");
var l4 = new Geometry.line(context, 550, 100, 550, 400, "red");
l1.draw();
l2.draw();
l3.draw();
l4.draw();
//drawing wave
sin(100, 250);
cos(550, 250);
//writing something on canvas
context.scale(1, -1);
context.fillStyle = "blue";
context.font = "22pt cursive";
context.fillText("sin wave", -400, 150);
context.fillText("cos wave", 150, 150);
context.restore();
function sin(x, y) {
    context.translate(x, y);
    context.beginPath();
    for (var i = 0; i <= 360; i++) {
        context.lineTo(i, 100 * Math.sin(i * Math.PI / 180));
    }
    context.lineWidth = 2;
    context.strokeStyle = "blue";
    context.stroke();
    context.restore();
}
function cos(x, y) {
    context.translate(x, y);
    context.beginPath();
    for (var i = 0; i <= 360; i++) {
        context.lineTo(i, 100 * Math.cos(i * Math.PI / 180));
        console.log(i, 100 * Math.cos(i * Math.PI / 180));
    }
    context.lineWidth = 2;
    context.strokeStyle = "blue";
    context.stroke();
    context.restore();
}
//# sourceMappingURL=app.js.map