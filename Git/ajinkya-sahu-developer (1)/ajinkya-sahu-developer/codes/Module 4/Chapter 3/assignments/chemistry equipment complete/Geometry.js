var Geometry;
(function (Geometry) {
    class point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    Geometry.point = point;
    class circle {
        constructor(context, pt1, r, color, pt2) {
            this.pt1 = pt1;
            this.pt2 = pt2;
            this.context = context;
            this.ang = 0;
            this.up1 = true;
            this.up2 = false;
            this.down1 = false;
            this.down2 = false;
            this.r = r;
            this.color = color;
        }
        draw() {
            this.context.beginPath();
            this.context.arc(this.pt1.x, this.pt1.y, this.r, 2.5 / 4 * Math.PI, 85 / 180 * Math.PI, false);
            this.context.lineWidth = 2;
            this.context.fillStyle = this.color;
            this.context.fill();
            //this.context.strokeStyle = "black";
            this.context.stroke();
        }
    }
    Geometry.circle = circle;
    class line {
        constructor(context, point1, point2, color) {
            this.pt1 = point1;
            this.pt2 = point2;
            this.context = context;
            this.color = color;
            this.up = true;
            this.down = false;
        }
        draw() {
            this.context.beginPath();
            this.context.moveTo(this.pt1.x, this.pt1.y);
            this.context.lineTo(this.pt2.x, this.pt2.y);
            this.context.lineWidth = 2;
            this.context.strokeStyle = this.color;
            this.context.stroke();
        }
    }
    Geometry.line = line;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=Geometry.js.map