var Geometry;
(function (Geometry) {
    class Point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    Geometry.Point = Point;
    class Circle {
        constructor(p1, r, color, b, context) {
            this.p1 = p1;
            this.r = r;
            this.color = color;
            this.context = context;
            this.g = 0;
            this.b = b;
        }
        draw() {
            this.context.beginPath();
            this.context.arc(this.p1.x, this.p1.y, this.r, 0, 2 * Math.PI);
            this.context.lineWidth = 4;
            this.context.fillStyle = this.color;
            this.context.fill();
            this.context.stroke();
        }
        update() {
            this.g += 1;
            this.p1.y += this.g;
            var d = canvas.height - this.r;
            if (this.p1.y > d) {
                this.p1.y = d;
                this.g = -(this.g * this.b);
            }
        }
    }
    Geometry.Circle = Circle;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map