var canvas:HTMLCanvasElement =<HTMLCanvasElement> document.getElementById("mycanvas");
var context:CanvasRenderingContext2D =<CanvasRenderingContext2D> canvas.getContext("2d");

var p1:HTMLImageElement = <HTMLImageElement> document.getElementById("pic1");
var p2:HTMLImageElement = <HTMLImageElement> document.getElementById("pic2");

var value;

canvas.addEventListener("click",circle);

function circle(e:MouseEvent){

    if(value){
        context.drawImage(p1,e.clientX,e.clientY);
    }
    else if(value==false){
        context.drawImage(p2,e.clientX,e.clientY);
    }
    
}

function pic1() {
    value = true;
}

function pic2() {
    value = false;
}