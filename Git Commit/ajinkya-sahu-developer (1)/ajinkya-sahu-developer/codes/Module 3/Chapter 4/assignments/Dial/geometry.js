var Geometry;
(function (Geometry) {
    class line {
        constructor(context, pt1x, pt1y, pt2x, pt2y, color) {
            this.pt1x = pt1x;
            this.pt2x = pt2x;
            this.pt1y = pt1y;
            this.pt2y = pt2y;
            this.context = context;
            this.color = color;
        }
        draw() {
            this.context.beginPath();
            this.context.moveTo(this.pt1x, this.pt1y);
            this.context.lineTo(this.pt2x, this.pt2y);
            this.context.lineWidth = 4;
            this.context.strokeStyle = this.color;
            this.context.stroke();
        }
    }
    Geometry.line = line;
    class circle {
        constructor(context, x, y, r, color) {
            this.pt1 = x;
            this.pt2 = y;
            this.context = context;
            this.ang = 0;
            this.r = r;
            this.color = color;
        }
        draw() {
            this.context.beginPath();
            this.context.arc(this.pt1, this.pt2, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 2;
            //this.context.fillStyle = this.color;
            //this.context.fill();
            this.context.strokeStyle = this.color;
            this.context.stroke();
        }
    }
    Geometry.circle = circle;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map