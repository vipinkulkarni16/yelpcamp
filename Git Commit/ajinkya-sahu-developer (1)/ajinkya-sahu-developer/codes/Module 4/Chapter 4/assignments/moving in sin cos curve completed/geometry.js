var Geometry;
(function (Geometry) {
    class point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    Geometry.point = point;
    class closePath {
        constructor(x, y) {
            this.vx = x;
            this.vy = y;
        }
    }
    Geometry.closePath = closePath;
    class circle {
        constructor(context, pt1, r, color) {
            this.ang = 0;
            this.pt1 = pt1;
            this.context = context;
            this.r = r;
            this._color = color;
        }
        draw() {
            this.context.beginPath();
            this.context.arc(this.pt1.x + 200, this.pt1.y + 300, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 2;
            this.context.fillStyle = this._color;
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
            this.update();
        }
        update() {
            this.ang++;
            var x = this.ang;
            var y = 100 * Math.sin(-this.ang * Math.PI / 180);
            this.pt1 = new point(x, y);
            console.log(x, y);
            if (this.ang == 360) {
                this.ang = 1;
            }
        }
    }
    Geometry.circle = circle;
    class sin {
        constructor(context, pt1, color) {
            this.ang = 0;
            this.pt1 = pt1;
            this.context = context;
            this._color = color;
        }
        draw() {
            this.context.save();
            this.context.translate(this.pt1.x, this.pt1.y);
            this.context.scale(1, -1);
            this.context.beginPath();
            this.context.moveTo(0, 0);
            for (let i = 0; i <= 360; i++) {
                this.context.lineTo(i, 100 * Math.sin(i * Math.PI / 180));
            }
            this.context.lineWidth = 2;
            this.context.strokeStyle = this._color;
            this.context.stroke();
            this.context.beginPath();
            this.context.moveTo(0, 100);
            this.context.lineTo(0, -100);
            this.context.moveTo(0, 0);
            this.context.lineTo(400, 0);
            this.context.lineWidth = 2;
            this.context.strokeStyle = "black";
            this.context.stroke();
            this.context.restore();
        }
    }
    Geometry.sin = sin;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map