class select_mat_type
{
    private list:HTMLSelectElement; 
    private list1:{value:number}[];
    public mat_type:{value:number,name:string,m:string,n:string}[];
    private startValue:number;
    private endValue:number;
    

    constructor(selectId:HTMLSelectElement,valstart:number,valend:number)
    {

        this.list = selectId;
        this.list1 = [];
        this.startValue = valstart;
        this.endValue = valend;
        this.mat_type = [];
        

    }

    add() 
    {

        this.mat_type.push({
            value: 0,
            name: "1 * 1",
            m: "1",
            n: "1"
        })
        this.mat_type.push({
            value: 1,
            name: "1 * 2",
            m: "1",
            n: "2"
        })
        this.mat_type.push({
            value: 2,
            name: "2 * 2",
            m: "2",
            n: "2"
        })
        this.mat_type.push({
            value: 3,
            name: "2 * 3",
            m: "2",
            n: "3"
        })
        this.mat_type.push({
            value: 4,
            name: "3 * 2",
            m: "3",
            n: "2"
        })
        this.mat_type.push({
            value: 5,
            name: "3 * 3",
            m: "3",
            n: "3"
        })
        this.mat_type.push({
            value: 6,
            name: "3 * 4",
            m: "3",
            n: "4"
        })
        this.mat_type.push({
            value: 7,
            name: "4 * 3",
            m: "4",
            n: "3"
        })
        this.mat_type.push({
            value: 8,
            name: "4 * 4",
            m: "4",
            n: "4"
        })

        //printing the list of items....
        for (let i = 0; i < 9; i++) {
            
            var x:HTMLOptionElement =<HTMLOptionElement> document.createElement("option");
            x.text = this.mat_type[i].name;
            x.value = this.mat_type[i].value.toString();
            //x.m = mat_type[i].m;
            //x.n = mat_type[i].n;
            this.list.add(x);
        }
    }

}

class table 
{
    protected _table;
    public noofrows=0;
    public noofcolunm=0;
    private type;
    private id;
    protected value;

    //constructor of the class....
    constructor(table:HTMLTableElement,noofrows:number,noofcolunm:number,celldatatype:string,id:string,value:string){

        this._table = table;
        this.noofrows = noofrows;
        this.noofcolunm = noofcolunm;
        this.type = celldatatype;
        this.id = id;
        this.value=value;

    }
    //drawing the table....
    create() 
    {
        while(this._table.rows.length>0)    //clearing preformed rows...
        {
            this._table.deleteRow(0);
        }

        for(var i=0;i<this.noofrows;i++)
        {
            var row = this._table.insertRow();
            for(var j=0;j<this.noofcolunm;j++)
            {
                var cell = row.insertCell();
                var option:HTMLInputElement =<HTMLInputElement> document.createElement("input");
                option.value = this.value;
                option.type = this.type;
                option.id = this.id +i.toString() + j.toString();
                cell.appendChild(option);
                console.log(option.id);
            }
        }
    }
}


var table1:HTMLTableElement = <HTMLTableElement> document.getElementById("tb1");
var table2:HTMLTableElement = <HTMLTableElement> document.getElementById("tb2");
var table3:HTMLTableElement = <HTMLTableElement> document.getElementById("tb3");

//creating drop down on web page....
var select1:HTMLSelectElement =<HTMLSelectElement> document.getElementById("mat_a");
var mata = new select_mat_type(select1,1,9);
mata.add();

var select2:HTMLSelectElement =<HTMLSelectElement> document.getElementById("mat_b");
var matb = new select_mat_type(select2,1,9);
matb.add();




function matrix_a() {
    var select_option:HTMLSelectElement =<HTMLSelectElement> document.getElementById("mat_a");
    var selected_option:number =parseInt(select_option.value);
    var r;
    var c;

    for (var i = 0; i < 9; i++) {

        if (mata.mat_type[i].value == selected_option) {

            r = mata.mat_type[i].m;
            c = mata.mat_type[i].n;
            console.log(r);
            console.log(c);
        
        }

    }
    var A:table = new table(table1,r,c,"number","t","");
    A.create();

}

function matrix_b() {
    var select_option:HTMLSelectElement =<HTMLSelectElement> document.getElementById("mat_b");
    var selected_option:number =parseInt(select_option.value);
    var r;
    var c;

    for (var i = 0; i < 9; i++) {

        if (mata.mat_type[i].value == selected_option) {

            r = mata.mat_type[i].m;
            c = mata.mat_type[i].n;
            console.log(r);
            console.log(c);
        
        }

    }
    var B:table = new table(table2,r,c,"number","p","");
    B.create();

}

function multiply() {
    var r:HTMLSelectElement =<HTMLSelectElement> document.getElementById("mat_a");
    var mat1 = parseInt(r.value);
    var s:HTMLSelectElement =<HTMLSelectElement> document.getElementById("mat_b");
    var mat2 = parseInt(s.value);
    var r1;
    var r2;
    var c1;
    var c2;


    for (var i = 0; i < 9; i++) {

        if (mata.mat_type[i].value == mat1) {

            r1 = mata.mat_type[i].m;
            c1 = mata.mat_type[i].n;

            console.log(r1);
            console.log(c1);
        }

    }

    for (i = 0; i < 9; i++) {

        if (matb.mat_type[i].value == mat2) {

            r2 = matb.mat_type[i].m;
            c2 = matb.mat_type[i].n;

            console.log(r2);
            console.log(c2);
        }


    }
    
    while (table3.rows.length > 0 ){
        table3.deleteRow(0);
    }
    if (c1 == r2) {
        for (i = 0; i < parseInt(r1); i++) {
            var row = table3.insertRow();

            for (var j = 0; j < parseInt(c2); j++) {
                var cell = row.insertCell();
                var t = document.createElement("input");
                t.type = "text";
                t.id = "r" + i + j;
                var z = 0;

                for (var k = 0; k < parseInt(c1); k++) {
                    var x:HTMLInputElement =<HTMLInputElement> document.getElementById("t" + i.toString() + k.toString());
                    var x1 =parseInt(x.value);
                    var y:HTMLInputElement =<HTMLInputElement> document.getElementById("p" + k.toString() + j.toString());
                    var y1 =parseInt(y.value);
                    console.log(x1);
                    console.log(y1);
                    console.log(z);
                    z = z + y1 * x1;

                }
                t.value = z.toString();
                cell.appendChild(t);
            }
        }
    } else {
        alert("Choose the correct order of 2nd matrix." + " Matrix rule is no. of rows of matrix B should equal to coloumn of matrix A");
    }
}
