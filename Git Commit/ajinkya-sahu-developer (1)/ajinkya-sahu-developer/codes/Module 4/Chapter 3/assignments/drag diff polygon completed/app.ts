var canvas:HTMLCanvasElement =<HTMLCanvasElement> document.getElementById("mycanvas");
var context:CanvasRenderingContext2D =<CanvasRenderingContext2D> canvas.getContext("2d");

canvas.addEventListener("click",click,false);
canvas.addEventListener("mousemove",move,false);
canvas.addEventListener("mouseup",moveup,false);
canvas.addEventListener("mousedown",movedown,false);
var rect = canvas.getBoundingClientRect();

var s1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
var choice:string;
function circle1() 
{
    choice="circle";
}
function polygon1() 
{
    var s=parseInt(s1.value);
    if( s>=3 && isNaN(s)!=true ){
        choice="polygon";
    }
    else{
        alert("Entered no is not greater than 2 or it is not a number")
    }
}
function drag(){
    choice="drag";
}

var cc:{circle:circle}[]=[];
var pc:{poly:polygon}[]=[];


function click(e:MouseEvent)
{
    if(choice=="circle")
    {
        var c1 = new point(e.clientX-rect.x,e.clientY-rect.y);
        var cir = new circle(context,c1, 30, "red");
        cir.draw();
        cc.push({circle:cir});

    }else if(choice=="polygon")
    {
        var c1 = new point(e.clientX-rect.x,e.clientY-rect.y);
        var s=parseInt(s1.value);
        var pl= new polygon(context,c1,s,"blue");
        pl.draw();
        pc.push({poly:pl});
    }
}

var enable:boolean=false;
function movedown(e:MouseEvent){
    if(choice=="drag"){
        enable=true;
    }
}
function moveup(e:MouseEvent){
    if(choice=="drag"){
        enable=false;
    }
}
function move(e:MouseEvent){
    if(enable){
        for(let i=0;i<cc.length;i++){
            if(cc[i].circle.isinside(new point(e.clientX-rect.x,e.clientY-rect.y))){
                cc[i].circle.cenpoint=new point(e.clientX-rect.x,e.clientY-rect.y);
                break;
            }
        }
        for(let i=0;i<pc.length;i++){
            if(pc[i].poly.isinside(new point(e.clientX-rect.x,e.clientY-rect.y))){
                pc[i].poly.startpoint=new point(e.clientX-rect.x,e.clientY-rect.y);
                break;
            }
        }
        context.clearRect(0,0,canvas.width,canvas.height);
        for(let i=0;i<cc.length;i++){
            cc[i].circle.draw();
        }
        for(let i=0;i<pc.length;i++){
            pc[i].poly.draw();
        }
    }
}