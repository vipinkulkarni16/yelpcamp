namespace Geometry{
    export class point {

        public x:number;
        public y:number;

        constructor(x:number,y:number){
            this.x=x;
            this.y=y;
        }
    }

    export class line {

        public pt1:point;
        public pt2:point;
        public context:CanvasRenderingContext2D;
        private ang:number;
        public color:string;
        private _ang:number;

        constructor(context:CanvasRenderingContext2D,point1:point,point2:point,color:string){

            this.pt1 = point1;
            this.pt2 = point2;
            this.context = context;
            this.color=color;
            this._ang = Math.atan2(this.pt2.y-this.pt1.y,this.pt2.x-this.pt1.x);

        }

        draw()
        {
            console.log("wrk");
            this.context.beginPath();
            this.context.moveTo(this.pt1.x, this.pt1.y);
            this.context.lineTo(this.pt2.x, this.pt2.y);
            this.context.lineWidth = 2;
            this.context.strokeStyle = this.color;
            this.context.stroke();
            this.update();
        }

        update()
        {
            this._ang++;
            console.log(this._ang);
            var len = Math.sqrt(Math.pow(this.pt1.x - this.pt2.x, 2) + Math.pow(this.pt1.y - this.pt2.y, 2));
            console.log(len);
            this.pt2.x = this.pt1.x + len * Math.cos(this._ang * Math.PI / 180);
            this.pt2.y = this.pt1.y + len * Math.sin(this._ang * Math.PI / 180);
            console.log(this.pt2.x, this.pt2.y);
            if (this._ang >= 360) 
            {
                this.ang = 0;
            }
        }


    }
}    