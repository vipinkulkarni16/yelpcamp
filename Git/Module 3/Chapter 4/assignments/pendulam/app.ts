var canvas: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context: CanvasRenderingContext2D = canvas.getContext("2d");

context.translate(0,600);
context.scale(1,-1);


var p1 = new Geometry.point(260,300);
var p2 = new Geometry.point(340,300);

var l1 = new Geometry.line(context,p1,p2,"black");
l1.draw();

var p3 = new Geometry.point(300,300);
var p4 = new Geometry.point(300,220);

var l2 = new Geometry.line(context,p3,p4,"black");
l2.draw();

var p5 = new Geometry.point(300,300);
var p6 = new Geometry.point(300,200);

var c1 = new Geometry.circle(context,p6,p5,20,"red");
c1.draw();


function ajink(){

    var ang = +prompt("Enter a angle between 0 to 180 degree from positive x axis");
    if( (ang > 180) || (ang < 0) || (isNaN(ang)!=false)){
        var ang = +prompt("enter a valid choice");  
    }

    context.clearRect(0,0,canvas.width,canvas.height);
    l1.draw();

    var x1 = 300 -100*Math.cos(ang * Math.PI / 180);
    var y1 = 300 -100*Math.sin(ang * Math.PI / 180);
    
    var x2 = 300 -80*Math.cos(ang * Math.PI / 180);
    var y2 = 300 -80*Math.sin(ang * Math.PI / 180);
    
    var p8 = new Geometry.point(x2,y2);
    var l3 = new Geometry.line(context,p3,p8,"black");
    var p7 = new Geometry.point(x1,y1);
    var c2 = new Geometry.circle(context,p7,p3,20,"red");
    l3.draw();
    c2.draw();

}