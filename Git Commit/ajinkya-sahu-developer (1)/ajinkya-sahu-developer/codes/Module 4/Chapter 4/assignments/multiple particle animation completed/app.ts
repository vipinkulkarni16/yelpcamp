var canvas:HTMLCanvasElement =<HTMLCanvasElement> document.getElementById("mycanvas");
var context:CanvasRenderingContext2D =<CanvasRenderingContext2D> canvas.getContext("2d");

context.save();
var c:Geometry.circle[]= [];
context.translate(0,400);
context.scale(1,-1);

var p1 = new Geometry.point(0,-12);
var p2 = new Geometry.point(canvas.width,-12);
var p3 = new Geometry.point(0,330);
var p4 = new Geometry.point(canvas.width,330);

var l1 = new Geometry.line(context,p1,p2,"Black");
var l2= new Geometry.line(context,p3,p4,"Black");
l1.draw();
l2.draw();

for (let i = 1;i<80;i++){
    var p = new Geometry.point(random(0,600),random(0,300));
    var circle = new Geometry.circle(context,p,10);
    circle.draw();
    c.push(circle);
    //console.log(c);
}

anim();

function anim(){
    
    context.clearRect(-10,-10,canvas.width + 10,canvas.height + 10);
    l1.draw();
    l2.draw();
    for(let i= 0;i<c.length;i++){
        c[i].draw();
        c[i].update(canvas);
    }
    window.requestAnimationFrame(anim);
}

function random(min:number,max:number){
    return Math.round(Math.random() * (max - min) + min);
}