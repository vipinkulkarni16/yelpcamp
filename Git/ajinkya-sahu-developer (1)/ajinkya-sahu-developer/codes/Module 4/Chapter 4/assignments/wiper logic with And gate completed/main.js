var Geometry;
(function (Geometry) {
    class point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    Geometry.point = point;
    class circle {
        constructor(context, pt1, pt2, r, color) {
            this.pt1 = pt1;
            this.pt2 = pt2;
            this.context = context;
            this.ang = 0;
            this.up1 = true;
            this.up2 = false;
            this.down1 = false;
            this.down2 = false;
            this.r = r;
            this.color = color;
        }
        draw() {
            this.context.beginPath();
            this.context.arc(this.pt1.x, this.pt1.y, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 2;
            this.context.fillStyle = this.color;
            this.context.fill();
            //this.context.strokeStyle = "black";
            this.context.stroke();
        }
        updateangle() {
            this.ang++;
            var len = Math.sqrt(Math.pow(this.pt1.x - this.pt2.x, 2) + Math.pow(this.pt1.y - this.pt2.y, 2));
            this.pt1.x = this.pt2.x + len * Math.cos(this.ang * Math.PI / 180);
            this.pt1.y = this.pt2.y + len * Math.sin(this.ang * Math.PI / 180);
            console.log(this.pt2.x, this.pt2.y);
        }
    }
    Geometry.circle = circle;
    class rectangle {
        constructor(context, pt1, length, width, color) {
            this.context = context;
            this.pt1 = pt1;
            this.l = length;
            this.w = width;
            this.ang = 0;
            this.color = color;
            this.up1 = true;
            this.up2 = false;
            this.down1 = false;
            this.down2 = false;
        }
        draw() {
            this.context.beginPath();
            this.context.rect(this.pt1.x, this.pt1.y, this.l, this.w);
            this.context.lineWidth = 2;
            this.context.fillStyle = this.color;
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
        }
    }
    Geometry.rectangle = rectangle;
    class line {
        constructor(context, point1, point2, color) {
            this.pt1 = point1;
            this.pt2 = point2;
            this.context = context;
            this.ang = -20;
            this.color = color;
            this.up = true;
            this.down = false;
        }
        draw() {
            this.context.beginPath();
            this.context.moveTo(this.pt1.x, this.pt1.y);
            this.context.lineTo(this.pt2.x, this.pt2.y);
            this.context.lineWidth = 2;
            this.context.strokeStyle = this.color;
            this.context.stroke();
        }
        updateangle() {
            var len = Math.sqrt(Math.pow(this.pt1.x - this.pt2.x, 2) + Math.pow(this.pt1.y - this.pt2.y, 2));
            if (this.ang < 20 && this.up) {
                this.ang++;
                this.pt2.x = this.pt1.x + len * Math.cos(this.ang * Math.PI / 180);
                this.pt2.y = this.pt1.y + len * Math.sin(this.ang * Math.PI / 180);
                console.log(this.pt2.x, this.pt2.y);
            }
            if (this.ang == 20) {
                this.up = false;
                this.down = true;
            }
            if (this.ang >= -20 && this.down) {
                this.ang--;
                this.pt2.x = this.pt1.x + len * Math.cos(this.ang * Math.PI / 180);
                this.pt2.y = this.pt1.y + len * Math.sin(this.ang * Math.PI / 180);
                console.log(this.pt2.x, this.pt2.y);
            }
            if (this.ang == -20) {
                this.up = true;
                this.down = false;
            }
        }
    }
    Geometry.line = line;
    class arc {
        constructor(context, c1, c2, radius) {
            this.context = context;
            this.c1 = c1;
            this.c2 = c2;
            this.r = radius;
        }
        draw() {
            this.context.beginPath();
            this.context.arc(this.c1, this.c2, this.r, -Math.PI / 2, Math.PI / 2);
            this.context.lineWidth = 2;
            this.context.strokeStyle = "black";
            this.context.stroke();
        }
    }
    Geometry.arc = arc;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=main.js.map