namespace Geometry{
    export class point {

        public x:number;
        public y:number;

        constructor(x:number,y:number){
            this.x=x;
            this.y=y;
        }
    }

    export class circle{
        private ang:number;
        public pt1:point;
        public pt2:point;
        public context:CanvasRenderingContext2D;
        public r:number;
        public color: string;

        private up1:Boolean;
        private up2:Boolean;
        private down1:Boolean;
        private down2:Boolean;

        constructor(context:CanvasRenderingContext2D,pt1:point,pt2:point,r:number,color:string){

            this.pt1 = pt1;
            this.pt2 = pt2;
            this.context = context;
            this.ang =0;
            this.up1=true;
            this.up2=false;
            this.down1=false;
            this.down2=false;
            this.r=r;
            this.color=color;
            
        }

        draw(){

            this.context.beginPath();
            this.context.arc(this.pt1.x, this.pt1.y, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 2;
            this.context.fillStyle = this.color;
            this.context.fill();
            //this.context.strokeStyle = "black";
            this.context.stroke();
        
        }

        updateangle(){
            this.ang++;
            var len = Math.sqrt(Math.pow(this.pt1.x - this.pt2.x, 2) + Math.pow(this.pt1.y - this.pt2.y, 2));
            this.pt1.x = this.pt2.x + len * Math.cos(this.ang * Math.PI / 180);
            this.pt1.y = this.pt2.y + len * Math.sin(this.ang * Math.PI / 180);
            console.log(this.pt2.x, this.pt2.y)
        }


    
    }

    export class rectangle{

        private ang:number;
        public pt1:point;
        public context:CanvasRenderingContext2D;
        public l:number;
        public w:number;
        public color:string;
        

        private up1:Boolean;
        private up2:Boolean;
        private down1:Boolean;
        private down2:Boolean;

        constructor(context:CanvasRenderingContext2D,pt1:point,length:number,width:number,color:string){

            this.context = context;
            this.pt1 = pt1;
            this.l=length;
            this.w=width;
            this.ang =0;
            this.color = color;

            this.up1=true;
            this.up2=false;
            this.down1=false;
            this.down2=false;
            
        }
    
        draw(){

            this.context.beginPath();
            this.context.rect(this.pt1.x, this.pt1.y, this.l, this.w);
            this.context.lineWidth = 2;
            this.context.fillStyle = this.color;
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();   

        }

    }

    export class line {

        public pt1:point;
        public pt2:point;
        public context:CanvasRenderingContext2D;
        private ang:number;
        public color:string;
        private up:boolean;
        private down:boolean;

        constructor(context:CanvasRenderingContext2D,point1:point,point2:point,color:string){

            this.pt1 = point1;
            this.pt2 = point2;
            this.context = context;
            this.ang = -20;
            this.color=color;
            this.up = true;
            this.down = false;

        }

        draw(){

        this.context.beginPath();
        this.context.moveTo(this.pt1.x, this.pt1.y);
        this.context.lineTo(this.pt2.x, this.pt2.y);
        this.context.lineWidth = 2;
        this.context.strokeStyle = this.color;
        this.context.stroke();

        }

        updateangle(){

        
        var len = Math.sqrt(Math.pow(this.pt1.x - this.pt2.x, 2) + Math.pow(this.pt1.y - this.pt2.y, 2));
        
        if (this.ang<20 && this.up){

        this.ang++;
        this.pt2.x = this.pt1.x + len * Math.cos(this.ang * Math.PI / 180);
        this.pt2.y = this.pt1.y + len * Math.sin(this.ang * Math.PI / 180);
        console.log(this.pt2.x, this.pt2.y);

        }

        if (this.ang==20){
            this.up=false;
            this.down=true;
        }
        
        if(this.ang>=-20 && this.down){

        this.ang--;
        this.pt2.x = this.pt1.x + len * Math.cos(this.ang * Math.PI / 180);
        this.pt2.y = this.pt1.y + len * Math.sin(this.ang * Math.PI / 180);
        console.log(this.pt2.x, this.pt2.y);

        }

        if(this.ang==-20){
            this.up=true;
            this.down=false;
        }


        }

    }

    export class arc{
        
        public c1:number;
        public c2:number;
        public r:number;
        public context:CanvasRenderingContext2D;

        constructor(context:CanvasRenderingContext2D,c1:number,c2:number,radius:number){

            this.context = context;
            this.c1 = c1;
            this.c2 = c2;
            this.r = radius;

        }

        draw(){
            this.context.beginPath();
            this.context.arc(this.c1, this.c2, this.r, -Math.PI/2 , Math.PI / 2);
            this.context.lineWidth = 2;
            this.context.strokeStyle = "black";
            this.context.stroke();
        }
    }
    
}